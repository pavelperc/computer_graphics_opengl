add_executable(task2
        common_header.h
        dirLight.cpp
        dirLight.h
        flyingCamera.cpp
        flyingCamera.h
        openGLControl.cpp
        openGLControl.h
        renderScene.cpp
        shaders.cpp
        shaders.h
        skybox.cpp
        skybox.h
        static_geometry.cpp
        static_geometry.h
        texture.cpp
        texture.h
        vertexBufferObject.cpp
        vertexBufferObject.h
        win_OpenGLApp.cpp
        win_OpenGLApp.h
        house/house_geom.h)

#set_target_properties(task2 PROPERTIES LINK_FLAGS /SUBSYSTEM:WINDOWS)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_SOURCE_DIR}/lib/FreeImage-3.18.0)
include_directories(${CMAKE_SOURCE_DIR}/lib/glm-0.9.9.7)
include_directories(${CMAKE_SOURCE_DIR}/lib/glew-2.1.0/include)


target_link_libraries(task2
        ${CMAKE_SOURCE_DIR}/lib/FreeImage-3.18.0/FreeImage.lib
        ${CMAKE_SOURCE_DIR}/lib/glew-2.1.0/glew32.lib
        ${CMAKE_SOURCE_DIR}/lib/glew-2.1.0/glew32s.lib
        glm
        )