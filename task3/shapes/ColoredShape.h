#pragma once

#include "Shape.h"

#include <glm/glm.hpp>

class ColoredShape : public Shape {

public:

    glm::vec3 color;

public:

    ColoredShape(glm::vec3 color) : color(color) {};

};