#include "Octahedron.h"

#include <vector>
#include <gl/glew.h>
#include <glm/glm.hpp>

void Octahedron::addFace(std::vector<glm::vec3> vertices, int i1, int i2, int i3) {
    glm::vec3& v1 = vertices[i1];
    glm::vec3& v2 = vertices[i2];
    glm::vec3& v3 = vertices[i3];
    
    glm::vec3 normal = glm::cross(v2 - v1, v3 - v1);
    
    vbo.AddData(&v1, sizeof(glm::vec3));
    vbo.AddData(&normal, sizeof(glm::vec3));
    vbo.AddData(&v2, sizeof(glm::vec3));
    vbo.AddData(&normal, sizeof(glm::vec3));
    vbo.AddData(&v3, sizeof(glm::vec3));
    vbo.AddData(&normal, sizeof(glm::vec3));
    verticesCount += 3;
}

Octahedron::Octahedron(glm::vec3 color) : ColoredShape(color) {

    std::vector<glm::vec3> vertices;
    vertices.push_back(glm::vec3(0, 1, 0));
    vertices.push_back(glm::vec3(1, 0, 0));
    vertices.push_back(glm::vec3(0, 0, 1));
    vertices.push_back(glm::vec3(-1, 0, 0));
    vertices.push_back(glm::vec3(0, 0, -1));
    vertices.push_back(glm::vec3(0, -1, 0));

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    vbo.CreateVBO();
    vbo.BindVBO(GL_ARRAY_BUFFER);
    
    addFace(vertices, 0, 2, 1);
    addFace(vertices, 0, 3, 2);
    addFace(vertices, 0, 4, 3);
    addFace(vertices, 0, 1, 4);
    addFace(vertices, 5, 1, 2);
    addFace(vertices, 5, 2, 3);
    addFace(vertices, 5, 3, 4);
    addFace(vertices, 5, 4, 1);

    vbo.UploadDataToGPU(GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) + sizeof(glm::vec3), 0);

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) + sizeof(glm::vec3), (void*)sizeof(glm::vec3));
}

void Octahedron::draw() {
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, verticesCount);
}
