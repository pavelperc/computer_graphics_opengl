#pragma once

#include "ColoredShape.h"

class Octahedron : public ColoredShape {

private:

    unsigned int verticesCount = 0;

    void addFace(std::vector<glm::vec3> vertices, int i1, int i2, int i3);
public:

    Octahedron(glm::vec3 color);

    void draw() override;

};