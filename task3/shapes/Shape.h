#pragma once

#include "../vertexBufferObject.h"

class Shape {

public:

    unsigned int vao;

    CVertexBufferObject vbo;

public:

    virtual void draw() = 0;

};