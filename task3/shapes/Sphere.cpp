#include "Sphere.h"

#include <vector>
#include <gl/glew.h>
#include <glm/glm.hpp>

Sphere::Sphere(glm::vec3 color) : ColoredShape(color) {
    const float PI = 3.141592653589793;

    const float R = 1;
    const int M = 10;
    const int N = 20;

    std::vector<glm::vec3> vertices;

    vertices.push_back(glm::vec3(0, R, 0));

    for (int i = 1; i <= M; i += 1) {
        float a = PI * (float)i / (float)(M + 1);

        float h = R * std::cos(a);
        float l = R * std::sin(a);

        for (int j = 0; j < N; j += 1) {
            float b = 2 * PI * (float)j / (float)N;

            vertices.push_back(glm::vec3(l * std::sin(b), h, l * std::cos(b)));
        }
    }

    vertices.push_back(glm::vec3(0, -R, 0));

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    vbo.CreateVBO();
    vbo.BindVBO(GL_ARRAY_BUFFER);

    glm::vec3 normal;

    for (int i = 1; i < N; i += 1) {
        normal = glm::cross(vertices[i] - vertices[0], vertices[i + 1] - vertices[0]);

        vbo.AddData(&vertices[0], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));
        vbo.AddData(&vertices[i], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));
        vbo.AddData(&vertices[i + 1], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));

        verticesCount += 3;
    }

    normal = glm::cross(vertices[N] - vertices[0], vertices[1] - vertices[0]);

    vbo.AddData(&vertices[0], sizeof(glm::vec3));
    vbo.AddData(&normal, sizeof(glm::vec3));
    vbo.AddData(&vertices[N], sizeof(glm::vec3));
    vbo.AddData(&normal, sizeof(glm::vec3));
    vbo.AddData(&vertices[1], sizeof(glm::vec3));
    vbo.AddData(&normal, sizeof(glm::vec3));

    verticesCount += 3;

    for (int i = 0; i < M - 1; i += 1) {
        int first = N * i + 1;
        int last = first + N - 1;

        for (int j = first; j < first + N - 1; j += 1) {
            normal = glm::cross(vertices[j + N] - vertices[j], vertices[j + 1] - vertices[j]);

            vbo.AddData(&vertices[j], sizeof(glm::vec3));
            vbo.AddData(&normal, sizeof(glm::vec3));
            vbo.AddData(&vertices[j + N], sizeof(glm::vec3));
            vbo.AddData(&normal, sizeof(glm::vec3));
            vbo.AddData(&vertices[j + 1], sizeof(glm::vec3));
            vbo.AddData(&normal, sizeof(glm::vec3));

            normal = glm::cross(vertices[j + N] - vertices[j + 1], vertices[j + N + 1] - vertices[j + 1]);

            vbo.AddData(&vertices[j + 1], sizeof(glm::vec3));
            vbo.AddData(&normal, sizeof(glm::vec3));
            vbo.AddData(&vertices[j + N], sizeof(glm::vec3));
            vbo.AddData(&normal, sizeof(glm::vec3));
            vbo.AddData(&vertices[j + N + 1], sizeof(glm::vec3));
            vbo.AddData(&normal, sizeof(glm::vec3));

            verticesCount += 6;
        }

        normal = glm::cross(vertices[last + N] - vertices[last], vertices[first] - vertices[last]);

        vbo.AddData(&vertices[last], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));
        vbo.AddData(&vertices[last + N], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));
        vbo.AddData(&vertices[first], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));

        normal = glm::cross(vertices[last + N] - vertices[first], vertices[first + N] - vertices[first]);

        vbo.AddData(&vertices[first], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));
        vbo.AddData(&vertices[last + N], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));
        vbo.AddData(&vertices[first + N], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));

        verticesCount += 6;
    }

    for (int i = (M - 1) * N + 1; i < M * N; i += 1) {
        normal = glm::cross(vertices[M * N + 1] - vertices[i], vertices[i + 1] - vertices[i]);

        vbo.AddData(&vertices[i], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));
        vbo.AddData(&vertices[M * N + 1], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));
        vbo.AddData(&vertices[i + 1], sizeof(glm::vec3));
        vbo.AddData(&normal, sizeof(glm::vec3));

        verticesCount += 3;
    }

    normal = glm::cross(vertices[M * N + 1] - vertices[M * N], vertices[(M - 1) * N + 1] - vertices[M * N]);

    vbo.AddData(&vertices[M * N], sizeof(glm::vec3));
    vbo.AddData(&normal, sizeof(glm::vec3));
    vbo.AddData(&vertices[M * N + 1], sizeof(glm::vec3));
    vbo.AddData(&normal, sizeof(glm::vec3));
    vbo.AddData(&vertices[(M - 1) * N + 1], sizeof(glm::vec3));
    vbo.AddData(&normal, sizeof(glm::vec3));

    verticesCount += 3;

    vbo.UploadDataToGPU(GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) + sizeof(glm::vec3), 0);

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) + sizeof(glm::vec3), (void*)sizeof(glm::vec3));
}

void Sphere::draw() {
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, verticesCount);
}
