#pragma once

#include "ColoredShape.h"

class Sphere : public ColoredShape {

private:

    unsigned int verticesCount = 0;

public:

    Sphere(glm::vec3 color);

    void draw() override;

};