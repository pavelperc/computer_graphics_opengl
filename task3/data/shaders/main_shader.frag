#version 330

smooth in vec2 vTexCoord;
smooth in vec3 vNormal;
smooth in vec3 vEyeSpacePos;
smooth in vec3 vWorldPos;
out vec4 outputColor;

uniform sampler2D gSampler;
uniform vec4 vColor;

#include "dirLight.frag"
#include "spotLight.frag"
#include "pointLight.frag"

uniform DirectionalLight sunLight;
uniform SpotLight spotLight;
uniform SpotLight spotLight2;
uniform PointLight pointLight;
uniform PointLight pointLight2;
uniform bool bOnlyColor;

void main()
{
	if (bOnlyColor) {
		outputColor = vColor;
	} else {
		vec3 vNormalized = normalize(vNormal);

		vec4 vTexColor = texture2D(gSampler, vTexCoord);
		vec4 vDirLightColor = getDirectionalLightColor(sunLight, vNormalized);
		vec4 vSpotlightColor = GetSpotLightColor(spotLight, vWorldPos);
		vec4 vSpotlight2Color = GetSpotLightColor(spotLight2, vWorldPos);
		vec4 vPointlightColor = getPointLightColor(pointLight, vWorldPos, vNormalized);
		vec4 vPointlight2Color = getPointLightColor(pointLight2, vWorldPos, vNormalized);
		outputColor = vColor * vTexColor*(vDirLightColor+vSpotlightColor+vPointlightColor + vPointlight2Color + vSpotlight2Color);
	}
}