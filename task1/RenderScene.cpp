#include "common_header.h"

#include "win_OpenGLApp.h"

#include "shaders.h"


UINT uiVBO[2];
UINT uiVAO[2];

CShader shVertex, shFragment;
CShaderProgram spMain;
const int starSize = 12;
const int treeSize = 12;

// Initializes OpenGL features that will be used.
// lpParam - Pointer to anything you want.
void InitScene(LPVOID lpParam)
{
	glClearColor(1.0f, 0.5f, 0.3f, 1.0f);
	
	// http://4.bp.blogspot.com/-SJTozDzskFM/VKUCVACKRrI/AAAAAAAAAxY/tBE3_e5VVK8/s1600/star_plot.png
	float starXY[starSize*2] = {
			0,0,
			//outer
			-300,70, 
			// inner etc
			-100,110,
			0,300,
			100,110,
			300,70,
			160,-90,
			190,-300,
			0,-210,
			-190,-300,
			-160,-90,
			-300,70,
	};
	float treeXY[treeSize*2] = {
			0,0,
			500,-500,
			-500,-500,
			0, -300,
			700, -1000,
			-700, -1000,
			0, -600,
			900, -1500,
			-900, -1500,
	};
	float fStar[starSize*6];
	float fTree[treeSize*6];
	
	for(int i = 0; i < starSize; i++) {
		float x = starXY[i * 2];
		float y = starXY[i * 2 + 1];
		bool inner = i % 2 == 0;
		
		fStar[i * 6] = x / 2000;
		fStar[i * 6 + 1] = y / 2000;
		fStar[i * 6 + 2] = 0.0;
		fStar[i * 6 + 3] = 1.0;
		fStar[i * 6 + 4] = inner ? 0.0f : 1.0f;
		fStar[i * 6 + 5] = 0.0;
	}
	
	for (int i = 0; i < treeSize; ++i) {
		float x = treeXY[i * 2];
		float y = treeXY[i * 2+ 1];
		
		fTree[i * 6] = x / 2000;
		fTree[i * 6 + 1] = y / 2000;
		fTree[i * 6 + 2] = 0.0f;
		fTree[i * 6 + 3] = 0.0f;
		fTree[i * 6 + 4] = 0.8f;
		fTree[i * 6 + 5] = 0.2f;
	}
	
	
    float fTriangle[] = {
            // xyz rgb
            -0.4f, 0.1f, 0.0f, 1.0f, 0.0f, 0.0f,
            0.4f, 0.1f, 0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.7f, 0.0f, 0.0f, 0.0f, 1.0f,
    };
	
	// Setup quad vertices
    float fQuad[] = {
            // xyz rgb
            -0.2f, -0.1f, 0.0f, 1.0f, 0.0f, 0.0f,
            -0.2f, -0.6f, 0.0f, 0.0f, 1.0f, 0.0f,
            0.2f, -0.1f, 0.0f, 0.0f, 0.0f, 1.0f,
            0.2f, -0.6f, 0.0f, 1.0f, 1.0f, 0.0f,
    };
	// Setup quad color

	glGenVertexArrays(2, uiVAO); // Generate two VAOs, one for triangle and one for quad
	glGenBuffers(2, uiVBO); // And 2 VBOs

	// Setup whole triangle
	glBindVertexArray(uiVAO[0]);

	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fStar), fStar, GL_STATIC_DRAW);
	
	// setup vao attributes: 3 floats for coords, 3 floats for color (with offset 3 floats)
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));

	// Setup whole quad
	glBindVertexArray(uiVAO[1]);

	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fTree), fTree, GL_STATIC_DRAW);
	
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));

	// Load shaders and create shader program

	shVertex.LoadShader("task1/data/shaders/shader.vert", GL_VERTEX_SHADER);
	shFragment.LoadShader("task1/data/shaders/shader.frag", GL_FRAGMENT_SHADER);

	spMain.CreateProgram();
	spMain.AddShaderToProgram(&shVertex);
	spMain.AddShaderToProgram(&shFragment);

	spMain.LinkProgram();
	spMain.UseProgram();
}

// Renders whole scene.
// lpParam - Pointer to anything you want.
void RenderScene(LPVOID lpParam)
{
	// Typecast lpParam to COpenGLControl pointer
	COpenGLControl* oglControl = (COpenGLControl*)lpParam;

	// We just clear color
	glClear(GL_COLOR_BUFFER_BIT);
	
	glBindVertexArray(uiVAO[1]);
	glDrawArrays(GL_TRIANGLES, 0, treeSize);

	glBindVertexArray(uiVAO[0]);
	glDrawArrays(GL_TRIANGLE_FAN, 0, starSize);
	
	oglControl->SwapBuffersM();
}

// Releases OpenGL scene.
// lpParam - Pointer to anything you want.
void ReleaseScene(LPVOID lpParam)
{
	spMain.DeleteProgram();

	shVertex.DeleteShader();
	shFragment.DeleteShader();
}